
var SEMANTIC_FORM_TEMPLATE = 
	'<form id="renform" class="anno">'+
        '<div id="renform-cancelatn-container" class="anno"><span id="renform-cncl-btn" class="anno" onclick="close_renform();" >&#10008;</span></div>'+
        '<h3 id="renform-header" class="anno">SEMANTIC OPERATIONS</h3><hr>'+
        '<label class="renform-labels anno">CUSTOM TAGS:&nbsp;&nbsp;</label>'+
        '<select id="custm-tag" class="options-list anno">'+
        	'<option value="All">All</option>'+
        	'<option value="Date">Date</option>'+
        	'<option value="Time">Time</option>'+
        	'<option value="Place">Place</option>'+
        	'<option value="Number">Number</option>'+
        	'<option value="user-defined-tag">User defined tag</option>'+
    	'</select>'+
		'<br><br>'+
		'<label class="renform-labels anno">CUSTOM SRC:&nbsp;&nbsp;</label>'+
        '<input id="custm-src" class="inputbox anno" type=text>'+
        '<br><br>'+
        '<label class="renform-labels anno">CUSTOM HANDLER:&nbsp;&nbsp;</label>'+
        '<input id="custm-handler" class="inputbox anno" type=text>'+
        '<br><br>'+
		'<input id="view-node-list" class="renform-btn anno" type="button" value="View-Nodes" onclick="view_node_list();">'+
        '<input id="clear-node-list" class="renform-btn anno" type="button" value="Clear-Nodes" onclick="clear_node_list();">'+
        '<input id="apply-actn-btn" class="renform-btn anno" type="button" value="Apply-Action" onclick="apply_semantics_actn();">'+
        '<input id="save-semantics-actn-btn" class="renform-btn anno" type="button" value="Save-Action" onclick="save_actn(this.id);">'+ 
        '<input id="reset-renform-btn" class="renform-btn anno" type="button" value="Reset-Form" onclick="reset_form();">'+
    '</form>'; 
