
var ADD_TEMPLATE = 
    '<label class="anno">ADD LOCATION:&nbsp;&nbsp;</label>'+
    '<select id="add-loc" class="anno">'+
        '<option class="anno" value="above">Above</option>'+
        '<option class="anno" value="below">Below</option>'+
    '</select>'+
    '<br><br>'+
    '<label class="anno">SOURCE LOCATION:&nbsp;&nbsp;</label>'+
    '<input id="src-loc" class="inputbox anno" type=text>';

var ADD_LINK_TEMPLATE =
    '<label class="anno">ADD LOCATION:&nbsp;&nbsp;</label>'+
    '<select id="add-loc" class="anno">'+
        '<option value="above">Above</option>'+
        '<option value="below">Below</option>'+
    '</select>'+
    '<br><br>'+
    '<label class="anno">SOURCE LOCATION:&nbsp;&nbsp;</label>'+
    '<input id="src-loc" class="inputbox anno" type=text>'+
    '<br><br>'+
    '<label class="anno">TEXT:&nbsp;&nbsp;</label>'+
    '<input id="link-txt" class="inputbox anno" type=text>';

var ADD_TOOLTIP_TEMPLATE =
    '<label class="anno">SOURCE LOCATION:&nbsp;&nbsp;</label>'+
    '<input id="src-loc" class="inputbox anno" type=text>';

var LANG_PRCS_TEMPLATE = 
	'<label class="anno">FROM TEXT:&nbsp;&nbsp;<span class="anno" id="from-txt"></span></label>'+
    '<select class="anno" id="select-from-lang">'+
        '<option value="en" class="anno">English</option>'+
        '<option value="hi" class="anno">Hindi</option>'+
        '<option value="te" class="anno">Telugu</option>'+
        '<option value="ta" class="anno">Tamil</option>'+
        '<option value="ml" class="anno">Malayalam</option>'+
        '<option value="ja" class="anno">Japanese</option>'+
        '<option value="zh" class="anno">Chinese</option>'+
    '</select>'+
    '<br><br>'+
    '<label class="anno">TO TEXT:&nbsp;&nbsp;<span class="anno" id="to-txt"></span></label>'+
    '<select class="anno" id="select-from-lang">'+
        '<option value="en" class="anno">English</option>'+
        '<option value="hi" class="anno">Hindi</option>'+
        '<option value="te" class="anno">Telugu</option>'+
        '<option value="ta" class="anno">Tamil</option>'+
        '<option value="ml" class="anno">Malayalam</option>'+
        '<option value="ja" class="anno">Japanese</option>'+
        '<option value="zh" class="anno">Chinese</option>'+
    '</select>';
