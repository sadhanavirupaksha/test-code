
from flask import Flask
from flask import request
from flask_cors import CORS
import requests as req
import bs4
import conf 

app = Flask(__name__)
CORS(app)

translatekey = 'trnsl.1.1.20160620T044235Z.009e3fdaf079e045.51ec20ede6d14038c2cb193de1f8891c28dfc749'

@app.route("/language-translive", methods=['POST'])
def languagetranslive():
    try:
        sentence = request.json['sentence']
    except:
        return "sentence parameter not passed"
    try :
        fromlang = request.json['from-language']
    except:
        return "en"
    try :
        tolang = request.json['to-language']
    except:
        return "to language not passed"

    res = req.get('https://translate.yandex.net/api/v1.5/tr/translate?key='+translatekey+'&text='+sentence+'&lang='+fromlang+'-'+tolang+'&format=plain&options=0');
    soup = bs4.BeautifulSoup(res.text)
    ret = soup.text
    return ret;

@app.route("/test", methods=['GET'])
def test():
    return "some test string"

if __name__ == '__main__':
    app.run(host=conf.HOST[0], port=conf.PORT[0], debug = True)
