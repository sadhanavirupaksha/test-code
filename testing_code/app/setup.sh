
#! bash script for setting up enviornment for flask app
sudo apt-get install python-virtualenv
virtualenv sss-maker-venv
sss-maker-venv/bin/pip install flask
sss-maker-venv/bin/pip install flask-wtf
sss-maker-venv/bin/pip install conf
sss-maker-venv/bin/pip install -U flask-cors
sss-maker-venv/bin/pip install pymongo
sss-maker-venv/bin/pip install bcrypt
sss-maker-venv/bin/pip install requests
sss-maker-venv/bin/pip install lxml
