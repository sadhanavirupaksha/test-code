
var event_list = {{ sss_event_list|tojson }};

window.onload = function(){
	document.getElementById('player').src = "{{ url_for('static', filename = 'sssren_welcomepage.html') }}";
}

var index = 0;
function renderMedia(){
   var new_src;
   if(index >= 0 & index < event_list.length)
      {
          console.log("inside if...");
          var cur_event = event_list[index];
          switch ( cur_event["media-type"] ) {
          case "video":
             console.log("switch...to video");
             new_src= cur_event["media-url"]+'&start='+cur_event["start"]+'&end='+cur_event["end"];
             break;
          case "pdf":
             console.log("switch...to pdf");
             new_src = "https://docs.google.com/viewer?srcid="+cur_event["media-url"]+"&pid=explorer&efh=false&a=v&chrome=false&embedded=true";
             break;
          case "ppt":
             console.log("switch...to ppt");
             new_src = cur_event["media-url"]+"&slide="+cur_event["start-slide"];
             break;
          default:
              alert("ERROR: Unkown Event was asked to be rendered");
          }
      }
       document.getElementById("player").src = new_src;
}

function nextSetSource() {
    
	index++;
	if(index == event_list.length){
        index = 0;
    }
    renderMedia();
}

function prevSetSource() {
    
	index--;
	if(index < 0){
        index = 0;
    }

	renderMedia();
}

function lastSetSource() {
    
	index = event_list.length-1;
	renderMedia();

}
