function date_format() {
    var items = document.body.getElementsByTagName("*");
    for (var i = 0, len = items.length; i < len; i++) {
        var str = items[i].innerText;
        var patt = new RegExp('[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$', 'g');
        var res = patt.exec(str);
        console.log(res);
        if(res !== "null"){
            var newstr = str.replace(res, new Date(res).toDateString("yyyy-MM-dd"));
            console.log(newstr);
            items[i].innerText = newstr;
        }  
    }    
}