from flask import Flask, request, render_template, flash, make_response, session, abort, jsonify, g, url_for, send_from_directory, \
    redirect
from flask_wtf import Form
from wtforms import StringField, IntegerField, SelectField, PasswordField, SubmitField, validators, ValidationError
from wtforms.fields.html5 import EmailField
from werkzeug.security import check_password_hash
from pymongo import MongoClient
from flask_cors import CORS, cross_origin
import conf
import json
import re
import bcrypt
import requests
from urllib.parse import unquote, quote
import urllib.request as urllib2
import lxml.html
from io import StringIO

app = Flask(__name__)
CORS(app)

app.config['SECRET_KEY'] = conf.SECRET_KEY[0]

client = MongoClient()
db = client[conf.DATABASE[0]]

class userLoginForm(Form):
    """
    Login form to access writing and settings pages
    """
    username = StringField('Username')
    password = PasswordField('Password')
    login = SubmitField("Login")

@app.route('/', methods=['GET', 'POST'])
def home():
    form = userLoginForm()
    """
    displays the urlform page if the user is already logged in
    else displays the login page.
    """
    if 'username' in session:
        return redirect(url_for('index'))
    else:
        return render_template('login.html', form = form)

@app.route('/login', methods=['GET', 'POST'])
def userLogin():
    error = None
    form = userLoginForm()
    """
    Compares current login info with the user profiles in database
    """
    users = db[conf.USERS_COLLECTION[0]]
    login_user = users.find_one({ "$and": [ { "userinfo.usrname": request.form["username"] }, { "userinfo.password": request.form["password"] } ] })

    if request.method == 'POST':
        """
        the code below is executed if, the entered credentials are valid and 
        if the user is a renarrator/admin then he/she will be redirected to an urlform page.
        """
        if login_user:
            user_details = users.find_one( { "userinfo.usrname": request.form["username"] } )
            type_of_user = user_details["userinfo"]["usrtype"]
            if (type_of_user == "RenarratorUser" or type_of_user == "AdminUser"): 
                session['username'] = user_details["userinfo"]["usrname"]
                session['userid'] = user_details["userinfo"]["usrid"]
                session['communityname'] = user_details["userinfo"]["cmntyname"]
                session['communityid'] = user_details["userinfo"]["cmntyid"]
                return redirect(url_for('index', message = session['username'] ))   
                """
                If user is not renarrator code below gets executed saying 
                that access denied
                """
            else:
                error = 'Access denied..!Only renarrator/admin user can login into sss-maker'
                return render_template('login.html', form=form, error=error)
                """
                the code below is executed if, the entered credentials are 
                invalid i.e., user will be in the same page.
                """         
        else:
            error = 'Invalid Credentials. Please try again'
            return render_template('login.html', form=form, error=error)

@app.route('/logout')
def logout():
    """
    Removes the username from the session
    if it exists
    """
    session.pop('username', None)
    return redirect(url_for('home'))

class registrationForm(Form):
    username = StringField('UserName', [validators.DataRequired(), validators.Length(min=4, max=25)])
    userid = StringField('UserId', [validators.DataRequired(), validators.Length(min=4, max=25)])
    usertype =  SelectField('Type of user', choices = [('RenarratorUser', 'Renarrator'), ('NormalUser', 'Normal user'), ('AdminUser', 'Admin')])
    email = EmailField('Email', [validators.DataRequired(), validators.Length(max=50), validators.Email()])
    communityname = StringField('CommunityName')
    communityid = StringField('CommunityId')
    password = PasswordField('Password', [validators.DataRequired(), validators.Length(max=15)])
    signup = SubmitField("Sign Up")

@app.route('/register', methods=['GET','POST'])
def userRegistration():
    form = registrationForm()
    if request.method == 'POST':
        """
        Checks if the user is already registered 
        or not from database
        """
        users = db[conf.USERS_COLLECTION[0]]
        existing_user = users.find_one({ "$or": [ { "userinfo.usrname": request.form["username"] }, { "userinfo.email": request.form["email"] } ] })
        if form.validate() == False:
            error = 'Please fill the mandatory fields and enter the details correctly..!'
            return render_template('registration.html', form = form, error = error)

        elif existing_user is None:
            hashpass = bcrypt.hashpw(request.form['password'].encode('utf-8'), bcrypt.gensalt())
            users.insert_one(
                {   
                    'userinfo': {
                        'usrname': request.form['username'],
                        'usrid': request.form['userid'],
                        'usrtype': request.form['usertype'],
                        'email': request.form['email'],
                        'cmntyname': request.form['communityname'],
                        'cmntyid': request.form['communityid'],
                        'password': request.form['password']
                    }
                }
            )
            flash('Thanks for registering.')
            return render_template('success.html')

        else:
            error = 'user name/email id already exists'
            return render_template('registration.html', form = form, error = error)
            
    elif request.method == 'GET':
        return render_template('registration.html', form = form)

class urlForm(Form):
    """
    url form to access writing and settings pages
    """
    url = StringField('Enter URL:', [validators.DataRequired(), validators.url() ])
    submit = SubmitField("Renarrate")

@app.route('/urlform', methods=['GET','POST'])
def index():
    form = urlForm()
    if request.method == 'GET':
        return render_template('index.html', form = form, username = session["username"])


@app.route('/openpage')
def startPage():
    d = {}
    d['foruri'] = request.args['foruri']
    myhandler1 = urllib2.Request(d['foruri'],
                                     headers={'User-Agent':
                                              "Mozilla/5.0 (X11; " +
                                              "Linux x86_64; rv:25.0)" +
                                              "Gecko/20100101 Firefox/25.0)"})
    # A fix to send user-agents, so that sites render properly.
    try:
        a = urllib2.urlopen(myhandler1)
        page = a.read()
        a.close()
    except urllib2.URLError:
        return 'Please check the URL you entered, maybe you have misspelled it.'
    try:
        page = str(page, 'utf-8')  # Hack to fix improperly displayed chars on wikipedia.
    except UnicodeDecodeError:
        pass   # Some pages may not need be utf-8'ed
    try:
        g.root = lxml.html.parse(StringIO(page)).getroot()
    except ValueError:
        g.root = lxml.html.parse(d['foruri']).getroot() # Sometimes creators of the page lie about the encoding, thus leading to this execption. http://lxml.de/parsing.html#python-unicode-strings
    g.root.make_links_absolute(d['foruri'], resolve_base_href=True)
    for i in g.root.iterlinks():
        if i[1] == 'href' and i[0].tag != 'link':
            try:
                i[0].attrib['href'] = 'http://{0}?foruri={1}'.format('127.0.0.1:8080/', quote(i[0].attrib['href']))
            except KeyError:
                i[0].attrib['href'] = '{0}?foruri={1}'.format('127.0.0.1:8080/', quote(i[0].attrib['href'].encode('utf-8')))
    #sendSessionDetails()
    setScripts()
    response = make_response()
    response.data = lxml.html.tostring(g.root)
    return response

def sendSessionDetails():
    sssdict_script =  g.root.makeelement('script')
    g.root.body.append(sssdict_script)
    sssdict_script.set('type', 'text/javascript')
    #sssdict_script.text = "var sss1 = {'metadata':{}, 'pgxform':[], 'ssscrite':{}};"+"sss1['metadata']['curratorname']"+"="+session['username']+";"+"sss['metadata']['curratorid']"+"="+session['userid']+";"+"sss['metadata']['cmntyname']"+"="+session['communityname']+";"+"sss['metadata']['cmntyid']"+"="+session['communityid']+";"

def setScripts():
    jq_script = g.root.makeelement('script')
    g.root.body.append(jq_script)
    jq_script.set('src', conf.JQUERY_URL[0])
    jq_script.set('type', 'text/javascript')

    fonts_link = g.root.makeelement('link')
    g.root.body.append(fonts_link)
    fonts_link.set('href', conf.FONTS_URL[0])
    fonts_link.set('rel', 'stylesheet')

    annolet_link = g.root.makeelement('link')
    g.root.body.append(annolet_link)
    annolet_link.set('href', conf.ANNOLET_STYLING_URL[0])
    annolet_link.set('rel', 'stylesheet')    

    annolet_script = g.root.makeelement('script')
    g.root.body.append(annolet_script)
    annolet_script.set('src', conf.ANNOLET_URL[0])
    annolet_script.set('type', 'text/javascript')

@app.route('/test', methods=['GET'])
def test():
    return "Its Working...!"

if __name__ == '__main__':
    app.run(host=conf.HOST[0], port=conf.PORT[0], debug = True)


    
