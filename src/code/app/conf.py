SECRET_KEY = ('dshgfcjsd',) # some long string
DATABASE = ('sss-dsl',) # Database where the user info and sss info is stored
USERS_COLLECTION= ['user-info'] # users info collection where, the users registration info in stored
HOST = ['127.0.0.1']
PORT = [8080]
# Libraries used by sss-annolet
JQUERY_URL = ['https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'] 
FONTS_URL = ['https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css']
ANNOLET_STYLING_URL = ['https://gl.githack.com/sadhanavirupaksha/test-code/raw/master/src/code/sss-annolet/css/annolet.css']
ANNOLET_URL = ['https://gl.githack.com/sadhanavirupaksha/test-code/raw/master/src/code/sss-annolet/annolet.js']
