var ANNOLET_TEMPLATE,
    STRUCT_FORM_TEMPLATE,
    STYLE_FORM_TEMPLATE,
    SEMANTIC_FORM_TEMPLATE,
    INFO_FORM_TEMPLATE,
    COMMON_ADD_TEMPLATE,
    ADD_LINK_TEMPLATE,
    ADD_TOOLTIP_TEMPLATE,
    LANG_PRCS_TEMPLATE,
    config_data;

var target_elems = [];
var sss = {"metadata":{"curratorid":"currator", "sssname":"some name"}, "pgxform":[], "ssscrite":{} };

get_config_data();

function get_config_data() {
    var config_url = "https://gl.githack.com/sadhanavirupaksha/test-code/raw/master/src/code/sss-annolet/config.json";
    
    $.getJSON(config_url, function(){
        console.log("Request to config file success..!");
    })
    .done(function(data, jqxhr, textStatus) {
        config_data = data;
        console.log("Retrieved the config data successfully..!");
        create_container("sticky-container");
        load_html("sticky-container", config_data["ANNOLET_GUI_URL"]);
        create_container("form-container");
        drag_element(document.getElementById(("form-container")));
        tooltip_to_getTags()
    })
    .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request to config file failed: " + ", " + err);
    })
    .always(function() {
        console.log("Request to config_file complete");
    });
}

function create_container(elmnt_id) {
    var body = document.getElementsByTagName("body")[0];
    var container = document.createElement("div");
    container.id = elmnt_id;
    container.className = "anno";
    body.appendChild(container);
}

function load_html(elmnt_id, template_url) {    
    $.getScript(template_url, function(){
        console.log("Request to script file success..!");
    })
    .done(function(script, jqxhr, textStatus) {
        switch(elmnt_id){
            case "sticky-container":
                $("#sticky-container").html(ANNOLET_TEMPLATE);
                break;

            case "struct_icon":
                $("#form-container").html(STRUCT_FORM_TEMPLATE);
                $("#struct-actns-list").change(function(){
                    load_html("struct-actns-attrs-list", config_data["STRUCT_ACTNS_ATTRS_URL"]);
                });
                break;

            case "style_icon":
                $("#form-container").html(STYLE_FORM_TEMPLATE);
                break;

            case "semantic_icon":
                $("#form-container").html(SEMANTIC_FORM_TEMPLATE);
                $("#semantics-domain-list").change(function(){
                    load_html("semantics-domain-attrs-list", config_data["SEMANTICS_DOMAIN_ATTRS_URL"]);
                });
                break;

            case "save_icon":
                $("#form-container").html(INFO_FORM_TEMPLATE);
                break;

            case "struct-actns-attrs-list":
                var selctd_actn = document.getElementById("struct-actns-list").value;
                if(selctd_actn === "addtxt"||selctd_actn === "addimg"||selctd_actn === "addyt"||selctd_actn === "addnote") {
                    $("#struct-actns-attrs-list").html(COMMON_ADD_TEMPLATE);
                }
                else if(selctd_actn === "addlink") {
                    $("#struct-actns-attrs-list").html(ADD_LINK_TEMPLATE);   
                }
                else if(selctd_actn === "lang-prcs") {
                    $("#struct-actns-attrs-list").html(LANG_PRCS_TEMPLATE);   
                }
                break;

            case "semantics-domain-attrs-list":
                var selctd_domain = document.getElementById("semantics-domain-list").value;
                if(selctd_domain === "num-domain") {
                    $("#semantics-domain-attrs-list").html(NUMBER_DOMAIN_ATTRS_TEMPLATE);
                }
                break;

            default:
                console.log("Error in load_html function, invalid id"+" "+elmnt_id+" "+"..!");
        }
        console.log("Loaded html in container..!");    
    })
    .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request to script file failed: " + ", " + err);    
    })
    .always(function() {
        console.log("Request to script file complete");
    });
}

function show_renform(elmnt_id) {
    var form = document.getElementById("form-container");
    form.style.display = "block";
    load_html(elmnt_id, config_data[elmnt_id.toUpperCase()+"_"+"FORM"+"_"+"URL"]);        
}

function close_renform() {
    var form = document.getElementById("form-container");
    form.style.display = "none";
}

function drag_element(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById("ren-form")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById("ren-form").onmousedown = drag_mouse_down;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = drag_mouse_down;
    }
    function drag_mouse_down(e) {
        e = e || window.event;
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = close_drag_element;
        // call a function whenever the cursor moves:
        document.onmousemove = element_drag;
    }
    function element_drag(e) {
        e = e || window.event;
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }
    function close_drag_element() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

function apply_struct_actn() {
    var actn_functns = {
        "addtxt": add_node,
        "addimg": addimg_node,
        "addyt": addyt_node,
        "addnote": addnote_node,
        "addlink": addlink_node,
        "lang-prcs": lang_translation
    };
    var selctd_actn = document.getElementById("struct-actns-list").value;
    switch(selctd_actn){
        case "addtxt": case "addimg": case "addyt": case "addnote":
            var loc = document.getElementById("add-loc").value;
            var src = document.getElementById("src-loc").value;
            if(src !== "") {
                actn_functns[selctd_actn](target_elems, selctd_actn, loc, src);    
            }else{
                alert("Enter all fields..!");
            }
            break;

        case "addlink": 
            var loc = document.getElementById("add-loc").value;
            var src = document.getElementById("src-loc").value;
            var link_txt = document.getElementById("link-txt").value;
            if(src !== "" && link_txt !== "") {
                actn_functns[selctd_actn](target_elems, selctd_actn, loc, src, link_txt);    
            }else{
                alert("Enter all fields..!");
            }
            break;

        case "lang-prcs":
            var from_lang = $("#select-from-lang").val();
            var to_lang = $("#select-to-lang").val();
            actn_functns[selctd_actn](target_elems, selctd_actn, from_lang, to_lang);
            break;

        default:
            console.log("Error in apply_struct_actn function, invalid id"+" "+elmnt_id+" "+"..!");
    } 
}

function add_node(src_xpath, struct_actn, add_loc, new_text){
    var span = document.createElement("span"); 
    span.className = "sss-ren";
    span.innerHTML = new_text;
    var xpath_list_length = src_xpath.length;
    for(var i=0; i<xpath_list_length; i++){
        var html_elem = document.getElementsByTagName(src_xpath[i]);
        var html_elem_list_len =  html_elem.length;
        for(var j=0; j<html_elem_list_len; j++) {
            if(html_elem[j].classList.contains("anno") === false) {
                if(add_loc === "below"){
                    $(span).insertAfter(html_elem[j]);
                }
                else{
                    $(span).insertBefore(html_elem[j]);
                }
            }
        }
    }
    alert(struct_actn+" "+"action applied on selected elements..!");
}

function addimg_node(src_xpath, sruct_actn, add_loc, img_src){
    var img_node = '<img src="'+img_src+'">';
    add_node(src_xpath, sruct_actn, add_loc, img_node);
}

function addyt_node(src_xpath, sruct_actn, add_loc, vdo_id){
    var embedHTML = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+vdo_id+'" frameborder="0" allowfullscreen></iframe>';
    add_node(src_xpath, sruct_actn, add_loc, embedHTML);
}

function addlink_node(src_xpath, sruct_actn, add_loc, url, text){
    var linkHTML = '<a href="'+url+'" target="_blank">'+text+'</a>';
    add_node(src_xpath, sruct_actn, add_loc, linkHTML);
}

function addnote_node(src_xpath, sruct_actn, add_loc, note_text){
    var cont_style ='width: 960px;margin: 100px auto;';
    var note_style = 'float:left;margin: auto;width: 300px;background: yellow;background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#EBEB00), to(#C5C500));background: -moz-linear-gradient(100% 100% 90deg, #C5C500, #EBEB00);padding: 20px 20px 20px 20px;-webkit-box-shadow: 0px 10px 30px #000;-moz-box-shadow: 0px 10px 30px #000';
    var noteHTML = '<div id="note_container" style="'+cont_style+'"><div class="stick_note" style="'+note_style+'"><p>'+note_text+'</div></div></div>';
    add_node(src_xpath, sruct_actn, add_loc, noteHTML);
}

function lang_translation(src_xpath, sruct_actn, from, to) {
    console.log("lang_translation");
    var xpath_list_length = src_xpath.length;
    for(var i=0; i<xpath_list_length; i++){
        var html_elem = document.getElementsByTagName(src_xpath[i]);
        var html_elem_list_len =  html_elem.length;
        console.log(html_elem_list_len);
        for(var j=0; j<html_elem_list_len; j++) {
            console.log("inside 2nd for loop");
            if(html_elem[j].classList.contains("anno") === false) {
                console.log(html_elem[j]);
                var html_elem_text = $(html_elem[j]).text();
                console.log(html_elem_text);
                $.ajax(
                    {
                        url: "http://127.0.0.1:4000/language-translive",
                        type: "POST",
                        data: JSON.stringify({"sentence":html_elem_text, "from-language":from, "to-language":to}),
                        dataType: "json",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            console.log(JSON.stringify(res));
                            console.log(res);
                            $(html_elem[j]).text(res);
                            alert(struct_actn+""+"action applied on selected elements..!");            
                        }
                    }
                );
            }
        }
    }
}

function apply_styling_actn() {
    var src_xpath = ["p"];
    var fontfamily = document.getElementById("fontfamily-list").value;
    var fontweight = document.getElementById("fontweight-list").value; 
    var fontstyle = document.getElementById("fontstyle-list").value; 
    var textcolor = document.getElementById("textcolors-list").value;
    var fontsize = document.getElementById("fontsize-list").value;
    var span = document.createElement("span"); 
    span.className = "sss-ren";
    span.setAttribute("style", "font-family:"+fontfamily+";"+"font-weight:"+fontweight+";"+"font-style:"+fontstyle+";"+"color:"+textcolor+";"+"font-size:"+fontsize+";");
    var xpath_list_length = src_xpath.length;
    for(var i=0; i<xpath_list_length; i++){
        var html_elem = document.getElementsByTagName(src_xpath[i]);
        var html_elem_list_len =  html_elem.length;
        for(var j=0; j<html_elem_list_len; j++) {
            if(html_elem[j].classList.contains("anno") === false) {
                $(html_elem[j]).wrap(span);
            }
        }
    }
    alert("Styling applied on selected elements..!");
}

function apply_semantics_actn() {
    var domain = document.getElementById("semantics-domain-list").value;
    if(domain === "num-domain") {
        var tag = document.getElementById("semantics-domain-tags-list").value;
        var custom_handler = document.getElementById("semantics-custom-handlers-list").value;
        if(custom_handler === "convert-handler") {
            if(tag === "date") {
                formatDate();
            }
        }
    }
}

function formatDate() {
    var selected_format = 'ar-EG';
    var all = document.body.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
        if(all[i].tagName == "DATE"){
            var tag_text = all[i].innerHTML;
            var num_to_format = new Date(tag_text);
            var formated_date = num_to_format.toLocaleDateString(selected_format)
            if (formated_date == "Invalid Date"){   
                console.log("Invalid date");
            }
            else {
                all[i].innerHTML=  "<span class='highlight' style='color:green'>"+formated_date+"</span>";
            }
        }
    }
    alert("Semantics applied to the juxtaposed tags..!");
}

function reset_changes(){
    //$(".sss-ren").remove();
}

function save_actn(elmnt_id) {
    var ac_pair_stmnt = {};
    var pgxform_stmnt = {
                            "loc":      {
                                        },
                            "ac_pair":  [
                                        ] 
                        };                       
    if(elmnt_id === "save-struct-actn") {
        save_struct_actn(ac_pair_stmnt, pgxform_stmnt);
    }
    else if(elmnt_id === "save-styling-actn") {
        save_styling_actn(ac_pair_stmnt, pgxform_stmnt);
    }
    else if(elmnt_id === "save-semantics-actn") {
        save_semantics_actn(ac_pair_stmnt, pgxform_stmnt);
    }        
}

function save_struct_actn(ac_pair, pgxform) {
    var selctd_actn = document.getElementById("struct-actns-list").value;
    switch(selctd_actn){
        case "addtxt": case "addimg": case "addyt": case "addnote":
            var src = document.getElementById('src-loc').value;
            var loc = document.getElementById('add-loc').value;
            if(src !== "") {
                ac_pair[selctd_actn] = {};
                ac_pair[selctd_actn]["srcloc"] = src;
                ac_pair[selctd_actn]["addloc"] = loc;
            }
            else {
                alert("Enter all fields..!");
            }
            break;

        case "addlink":
            var src = document.getElementById('src-loc').value;
            var loc = document.getElementById('add-loc').value;
            var link_txt = document.getElementById('link-txt').value;
            if(src !== "" && link_txt !== "") {
                ac_pair[selctd_actn] = {};
                ac_pair[selctd_actn]["srcloc"] = src;
                ac_pair[selctd_actn]["addloc"] = loc;
                ac_pair[selctd_actn]["text"] = linktxt;
            }
            else {
                alert("Enter all fields..!");   
            }
            break;

        case "lang-prcs":
            var from_lang = document.getElementById('select-from-lang').value;
            var to_lang = document.getElementById('select-to-lang').value;
            ac_pair[selctd_actn] = {};
            ac_pair[selctd_actn]["from_lang"] = from_lang;
            ac_pair[selctd_actn]["to_lang"] = from_lang;
            break;

        default:
            console.log("Error in save_struct_actn function, invalid id"+" "+elmnt_id+" "+"..!");
    }
    update_sss_pgxform(ac_pair, pgxform);
}

function save_styling_actn(ac_pair, pgxform) {
    var fontfamily = document.getElementById("fontfamily-list").value;
    var fontweight = document.getElementById("fontweight-list").value; 
    var fontstyle = document.getElementById("fontstyle-list").value; 
    var textcolor = document.getElementById("textcolors-list").value;
    var fontsize = document.getElementById("fontsize-list").value;
    ac_pair["modelm"] = {};
    ac_pair["modelm"]["fontfamily"] = fontfamily;
    ac_pair["modelm"]["fontweight"] = fontweight;
    ac_pair["modelm"]["fontstyle"] = fontstyle;
    ac_pair["modelm"]["textcolor"] = textcolor;
    ac_pair["modelm"]["fontsize"] = fontsize;
    update_sss_pgxform(ac_pair, pgxform);
}

function update_sss_pgxform(ac_pair, pgxform) {
    var pgxform_length = sss["pgxform"].length;
    if(pgxform_length === 0) {
        pgxform["loc"]["tag"] = target_elems;
        pgxform["ac_pair"].push(ac_pair);
        sss.pgxform.push(pgxform);
    }
    else {
        for (var i = 0; i <= pgxform_length; i++) {
            if (JSON.stringify(target_elems) === JSON.stringify(sss["pgxform"][i]["loc"]["tag"])) {
                console.log('They are equal!');
                sss["pgxform"][i]["ac_pair"].push(ac_pair);
                break;
            }
        }
    }
    alert("Saved action in ADM..!");
}

function update_sss_metadata(desc, name, id) {
    var urlParams = new URLSearchParams(window.location.search);
    sss.metadata["renurl"] = urlParams.get('foruri');   
    sss.metadata["sssdesc"] =  desc;
    sss.metadata["sssname"] = name;
    sss.metadata["sssid"] = id;
}

function update_sss_ssscrite(user, cmnty) {
    sss.ssscrite["usrname"] = user;
    sss.ssscrite["usrcmnty"] = cmnty;
}

function validate_sss_infoform() {
    var sss_desc = document.getElementById("sss-desc").value;
    var sss_name = document.getElementById("sss-name").value;
    var sss_id = document.getElementById("sss-id").value;
    var sss_username = document.getElementById("sss-username").value;
    var sss_cmntyname = document.getElementById("sss-cmntyname").value;
    if(sss_desc !== "" && sss_id !== "" && sss_username !== "" && sss_cmntyname !== "") {
        update_sss_metadata(sss_desc, sss_name, sss_id);
        update_sss_ssscrite(sss_username, sss_cmntyname);
        alert("Form is successfully submitted..! ☺️");
        alert(JSON.stringify(sss, null, 2));
        save_sss();
    }
    else{
        alert("All fields are required..!");
    }
}

function save_sss() {
    $.ajax(
        {
            url: "http://localhost:5000/api/update/sss/",
            type: "POST",
            data: JSON.stringify(sss),
            dataType: 'json',
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function(res) {
                console.log(JSON.stringify(res)); 
                alert("Changes on page are saved..! ☺️");
            }
        }
    );
}

function check_modifications(elmnt_id) {
    var pgxform_length = sss["pgxform"].length;
    if(pgxform_length === 0) {
        alert("No modifications to save...!");
    }
    else {
        show_renform(elmnt_id);
    }
}

function tooltip_to_getTags() {
    create_container("tooltip-container");
    document.getElementById("tooltip-container").innerHTML = 
        "<button id='add_tag' class='anno' onclick=add_selcted_Tags(); style='color:blue;'>Add Tag</button>"+
        "<button id='del_tag' class='anno' onclick=del_selcted_Tags(); style='color:blue;'>Del Tag</button>";

    var all = document.body.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
        if( all[i].classList.contains("anno") === false ) {
            console.log(all[i].className);
            all[i].addEventListener("mouseup", function(e) {
               get_content(e);
            });
        }
    }    
}

// function get_content(e) {
//     var current = get_selected_data();
//     if(typeof(current) == "object"){
//         console.log("Text selected...");
//         var x = e.pageX;
//         var y = e.pageY;
//         place_tooltip(x, y);
//         document.getElementById("tooltip-container").style.display = "block";
//     }
//     else if(typeof(current) == "string"){
//         console.log("Text not selected...");
//         document.getElementById("tooltip-container").style.display = "none";   
//     } 
// }

// function get_selected_data() { 
//     var selctd_data = {};
//     var parentEl = null, sel;
//     if(window.getSelection){
//         sel = window.getSelection();
//         if (sel.rangeCount) {
//             parentEl = sel.getRangeAt(0).commonAncestorContainer;
//             if (parentEl.nodeType != 1) {
//                 parentEl = parentEl.parentNode;
//                 selctd_data["text"] = parentEl["textContent"];
//                 selctd_data["text_node"] = parentEl["nodeName"];
//                 selctd_data["text_innerhtml"] = parentEl["innerHTML"];
//                 if(parentEl["id"] !== ""){
//                     selctd_data["text_id"] = parentEl[id];
//                 }
//                 else if(parentEl["className"] !== ""){
//                     selctd_data["text_class"] = parentEl[className];
//                 }
//             }
//         }
//         return selctd_data; 
//     } 
//     else if((sel = document.selection)){
//         parentEl = sel.createRange().parentElement();
//         selctd_data["text"] = parentEl["textContent"];
//         selctd_data["textnode"] = parentEl["nodeName"];
//         selctd_data["textnodetype"] = parentEl["nodeType"];
//         if(parentEl["id"] !== ""){
//             selctd_data["text_id"] = parentEl[id];
//         }
//         else if(parentEl["className"] !== ""){
//             selctd_data["text_class"] = parentEl[className];
//         }
//         return selctd_data; 
//     }
//     return ''; 
// }

function tooltip_to_getTags() {
    create_container("tooltip-container");
    document.getElementById("tooltip-container").innerHTML = 
        "<button id='add_tag' class='anno' onclick=add_selcted_Tags(); style='color:blue;'>Add Tag</button>"+
        "<button id='del_tag' class='anno' onclick=del_selcted_Tags(); style='color:blue;'>Del Tag</button>";

    var all = document.body.getElementsByTagName("*");
    for (var i=0, max=all.length; i < max; i++) {
        if( all[i].classList.contains("anno") === false ) {
            console.log(all[i].className);
            all[i].addEventListener("mouseup", function(e) {
               get_content(e);
            });
        }
    }    
}

function get_content(e) {
    var current = get_selected_data();
    // console.log(current);
    if(current["selctd_text"] !== "") {
        var x = e.pageX;
        var y = e.pageY;
        place_tooltip(x, y);
        document.getElementById("tooltip-container").style.display = "block";
    }
    else if(current["selctd_text"] === "") {
        document.getElementById("tooltip-container").style.display = "none";   
    } 
}

function get_selected_data(){ 
    var selctd_data = {"selctd_text":"", "selctdnode":"", "selctdnode_innerhtml":"", "selctdnode_id":"", "selctdnode_class":""};
    if(window.getSelection){
        selctd_data["selctd_text"] = window.getSelection().toString();
        if(window.getSelection().anchorNode["nodeType"] == 3){
            selctd_data["selctdnode"] = window.getSelection().anchorNode["parentNode"]["nodeName"];
            selctd_data["selctdnode_innerhtml"] = window.getSelection().anchorNode["parentNode"]["innerHTML"];
            selctd_data["selctdnode_id"] = window.getSelection().anchorNode["parentNode"]["id"];
            selctd_data["selctdnode_class"] = window.getSelection().anchorNode["parentNode"]["className"];
        }
        else if(window.getSelection().anchorNode["nodeType"] == 1){
            selctd_data["selctdnode"] = window.getSelection().anchorNode["nodeName"];
            selctd_data["selctdnode_innerhtml"] = window.getSelection().anchorNode["innerHTML"];
            selctd_data["selctdnode_id"] = window.getSelection().anchorNode["id"];
            selctd_data["selctdnode_class"] = window.getSelection().anchorNode["className"];
        }  
        return selctd_data; 
    } 
    else if(document.getSelection){
        return document.getSelection(); 
    } 
    else if(document.selection){
        return document.selection.createRange().text; 
    }
    return selctd_data; 
}

function place_tooltip(x_pos, y_pos) {
    var tooltip = document.getElementById("tooltip-container");
    tooltip.style.position = "absolute";
    tooltip.style.left = x_pos + "px";
    tooltip.style.top = y_pos + "px";
}

function add_selcted_Tags() {
    var current = get_selected_data();
    if(current["selctd_text"] !== "") {
        if(current["selctdnode_id"] !== "") {
            console.log(target_elems);
            target_elems.push("#"+current["selctdnode_id"]);
            var nodes_string = document.getElementById("selctd_nodes");
            nodes_string.innerHTML = target_elems;
        }
    }
    else if(current["selctd_text"] === "") {
        alert("Select the text..!"); 
    }   
}

function del_selcted_Tags() {

}

// function add_selcted_Tags() {
//     var current = get_selected_data();
//     var nodes_string = document.getElementById("selctd_nodes");
//     if(current["selctd_text"] !== "") {
//         if(current["selctdnode_id"] !== ""){
//             if(target_elems.indexOf("#"+current["selctdnode_id"]) !== -1){
//                 alert("Node already selected");
//             }
//             else{
//                 target_elems.push("#"+current["selctdnode_id"]);
//                 nodes_string.innerHTML = target_elems;
//             }
//         }
//         else if(current["selctdnode_class"] !== ""){
//             var all = document.body.getElementsByClassName(current["selctdnode_class"]);
//             for (var i=0, max=all.length; i < max; i++){
//                 if(all[i].innerHTML === current["selctdnode_innerhtml"]){
//                     target_elems.push(all[i].tagName.toLowerCase()+"."+current["selctdnode_class"]);
//                     nodes_string.innerHTML = target_elems;      
//                 }
//             }
//         }
//         else {
//             if(target_elems.indexOf(current["selctdnode"].toLowerCase()) !== -1){
//                 alert("Node already selected");
//             }
//             else {
//                 target_elems.push(current["selctdnode"].toLowerCase());
//                 nodes_string.innerHTML = target_elems;
//             }
//         }
//     }
//     else if(current["selctd_text"] === "") {
//         alert("Select the text..!"); 
//     }   
// }
