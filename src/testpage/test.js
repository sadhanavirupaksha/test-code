var ANNOLET_TEMPLATE,
    STRUCT_FORM_TEMPLATE,
    STYLE_FORM_TEMPLATE,
    SEMANTIC_FORM_TEMPLATE,
    INFO_FORM_TEMPLATE,
    COMMON_ADD_TEMPLATE,
    ADD_LINK_TEMPLATE,
    ADD_TOOLTIP_TEMPLATE,
    LANG_PRCS_TEMPLATE,
    config_data,
    path;

var target_elems_data = [];
var paths_list = [];
var sss = { "metadata":{"curratorid":"xyz"}, "pgxform":[], "ssscrite":{} };

get_config_data();

function get_config_data() {
    var config_url = "https://gl.githack.com/sadhanavirupaksha/test-code/raw/master/src/code/sss-annolet/config.json";
    
    $.getJSON(config_url, function(){
        console.log("Request to config file success..!");
    })
    .done(function(data, jqxhr, textStatus) {
        config_data = data;
        console.log("Retrieved the config data successfully..!");
        create_container("annolet-sticky-container");
        load_html("annolet-sticky-container", config_data["ANNOLET_GUI_URL"]);
        create_container("renform-container");
        drag_element(document.getElementById(("renform-container")));
        tooltip_to_selectlocators();
    })
    .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request to config file failed: " + ", " + err);
    })
    .always(function() {
        console.log("Request to config_file complete");
    });
}

function create_container(elmnt_id) {
    var body = document.getElementsByTagName("body")[0];
    var container = document.createElement("div");
    container.id = elmnt_id;
    container.className = "anno";
    body.appendChild(container);
}

function load_html(elmnt_id, template_url) {    
    $.getScript(template_url, function(){
        console.log("Request to script file success..!");
    })
    .done(function(script, jqxhr, textStatus) {
        switch(elmnt_id){
            case "annolet-sticky-container":
                $("#annolet-sticky-container").html(ANNOLET_TEMPLATE);
                break;

            case "struct_icon":
                $("#renform-container").html(STRUCT_FORM_TEMPLATE);
                $("#struct-actns-list").change(function(){
                    load_html("struct-actns-attrs-list", config_data["STRUCT_ACTNS_ATTRS_URL"]);
                });
                break;

            case "style_icon":
                $("#renform-container").html(STYLE_FORM_TEMPLATE);
                break;

            case "semantic_icon":
                $("#renform-container").html(SEMANTIC_FORM_TEMPLATE);
                break;

            case "save_icon":
                $("#renform-container").html(INFO_FORM_TEMPLATE);
                break;

            case "struct-actns-attrs-list":
                var selctd_actn = document.getElementById("struct-actns-list").value;
                if(selctd_actn === "addtxt"||selctd_actn === "addimg"||selctd_actn === "addyt"||selctd_actn === "addnote") {
                    $("#struct-actns-attrs-list").html(COMMON_ADD_TEMPLATE);
                }
                else if(selctd_actn === "addlink") {
                    $("#struct-actns-attrs-list").html(ADD_LINK_TEMPLATE);   
                }
                else if(selctd_actn === "lang-prcs") {
                    $("#struct-actns-attrs-list").html(LANG_PRCS_TEMPLATE);   
                }
                break;

            default:
                console.log("Error in load_html function, invalid id"+" "+elmnt_id+" "+"..!");
        }
        console.log("Loaded html in container..!");    
    })
    .fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Request to script file failed: " + ", " + err);    
    })
    .always(function() {
        console.log("Request to script file complete");
    });
}

function show_renform(elmnt_id) {
    var form = document.getElementById("renform-container");
    form.style.display = "block";
    load_html(elmnt_id, config_data[elmnt_id.toUpperCase()+"_"+"FORM"+"_"+"URL"]);        
}

function close_renform() {
    var form = document.getElementById("renform-container");
    form.style.display = "none";
}

function reset_form(){
    document.getElementById("renform").reset();
}

function drag_element(elmnt) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById("renform")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById("renform").onmousedown = drag_mouse_down;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = drag_mouse_down;
    }
    function drag_mouse_down(e) {
        e = e || window.event;
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = close_drag_element;
        // call a function whenever the cursor moves:
        document.onmousemove = element_drag;
    }
    function element_drag(e) {
        e = e || window.event;
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }
    function close_drag_element() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

function tooltip_to_selectlocators() {
    create_container("tooltip-container");
    document.getElementById("tooltip-container").innerHTML = 
        '<input id="select_node" class="anno" type="button" value="SELECT NODE" onclick=extract_elmnts();>';

    var all = document.body.getElementsByTagName("*");
    for(var i=0, max=all.length; i < max; i++){
        if( all[i].classList.contains("anno") === false ){
            all[i].addEventListener("mouseup", function(event) {
               get_content(event);
            });
        }
    }
}

function get_content(event) {
    var current = get_selected_text(event);
    if(current !== "") {
        var x = event.pageX;
        var y = event.pageY;
        place_tooltip(x, y);
        document.getElementById("tooltip-container").style.display = "block";
    }
    else if(current === "") {
        document.getElementById("tooltip-container").style.display = "none";   
    } 
}

function get_selected_text(event) {
    if (window.getSelection){
        console.log(window.getSelection());
        if (event===undefined) event= window.event;
        var target= 'target' in event? event.target : event.srcElement; 
        var root= document.compatMode==='CSS1Compat'? document.documentElement : document.body;
        path= getPathTo(target);
        return window.getSelection().toString();
    }
    else if (document.selection && document.selection.type != "Control"){
        return document.selection.createRange().text;
    };
}

function getPathTo(element) {
    if (element.id!=='')
        return 'id("'+element.id+'")';
    if (element===document.body)
        return "//"+element.tagName.toLowerCase();

    var ix= 0;
    var siblings= element.parentNode.childNodes;
    for (var i= 0; i<siblings.length; i++) {
        var sibling= siblings[i];
        if (sibling===element)
            return getPathTo(element.parentNode)+'/'+element.tagName.toLowerCase()+'['+(ix+1)+']';
        if (sibling.nodeType===1 && sibling.tagName===element.tagName)
            ix++;
    }
}

function place_tooltip(x_pos, y_pos) {
    var tooltip = document.getElementById("tooltip-container");
    tooltip.style.position = "absolute";
    tooltip.style.left = x_pos + "px";
    tooltip.style.top = y_pos + "px";   
}

function extract_elmnts() {
    if(window.getSelection){
        var text = window.getSelection().toString(); 
    }
    else if(document.selection && document.selection.type != "Control") 
    {
        var text = document.selection.createRange().text;
    };
    var temp = {};
    temp.xpath = path;
    temp.startPoint = window.getSelection().anchorOffset;
    temp.endPoint = parseInt(temp.startPoint)+text.length;
    temp.data = text;
    target_elems_data.push(temp);
    paths_list.push(path);
    alert(JSON.stringify(temp, null, 2));
    highlight_selection();
}

function highlight_selection() {
    var range = window.getSelection().getRangeAt(0);
    var span = document.createElement('span');
    span.setAttribute("style", "background-color: yellow;");
    span.appendChild(range.extractContents());
    range.insertNode(span);
}

function view_node_list() {
    if(target_elems_data.length > 0){
        alert(JSON.stringify(target_elems_data));
    }
    else{
        alert("Nodes are not yet selected..!");
    }
}

function clear_node_list() {
    while (target_elems_data.length > 0) {
        target_elems_data.pop();
    }
    while (paths_list.length > 0) {
        paths_list.pop();
    }
    alert("Nodes list removed, select the new nodes list to perform the actions");
}

function apply_struct_actn() {
    var actn_functns = {
        "addtxt": add_node,
        "addimg": addimg_node,
        "addyt": addyt_node,
        "addnote": addnote_node,
        "addlink": addlink_node,
        "lang-prcs": lang_translation
    };
    if(target_elems_data.length === 0){
        alert("Please select the Nodes..!");
    }
    else{
        var selctd_actn = document.getElementById("struct-actns-list").value;
        switch(selctd_actn){
            case "addtxt": case "addimg": case "addyt": case "addnote":
                var loc = document.getElementById("add-loc").value;
                var src = document.getElementById("src-loc").value;
                if(src === "") {
                    alert("Please enter all fields..!");
                }else{
                    actn_functns[selctd_actn](selctd_actn, loc, src);
                }
                break;

            case "addlink": 
                var loc = document.getElementById("add-loc").value;
                var src = document.getElementById("src-loc").value;
                var link_txt = document.getElementById("link-txt").value;
                if(src === "" || link_txt === "") {
                    alert("Please enter all fields..!");
                }else{
                    actn_functns[selctd_actn](selctd_actn, loc, src, link_txt);    
                }
                break;

            case "lang-prcs":
                var from_lang = $("#select-from-lang").val();
                var to_lang = $("#select-to-lang").val();
                if(from_lang === to_lang){
                    alert("Please check the lang transformation selection..!");
                }else{
                    actn_functns[selctd_actn](selctd_actn, from_lang, to_lang);
                }
                break;

            default:
                console.log("Error in apply_struct_actn function, invalid id"+" "+elmnt_id+" "+"..!");
        }
    }
}

function apply_styling_actn() {
    if(target_elems_data.length === 0){
        alert("Please select the Nodes..!");
    }
    else{
        var fontfamily = document.getElementById("fontfamily-list").value;
        var fontweight = document.getElementById("fontweight-list").value; 
        var fontstyle = document.getElementById("fontstyle-list").value; 
        var textcolor = document.getElementById("textcolors-list").value;
        var fontsize = document.getElementById("fontsize-list").value;
        var span = document.createElement("span"); 
        span.className = "sss-ren";
        span.setAttribute("style", "font-family:"+fontfamily+";"+"font-weight:"+fontweight+";"+"font-style:"+fontstyle+";"+"color:"+textcolor+";"+"font-size:"+fontsize+";");
        var xpath_data_len = target_elems_data.length;
        for(var i=0; i<xpath_data_len; i++){
            var elem = getElementByXpath(target_elems_data[i]["xpath"]);
            $(elem).wrap(span);   
        }
        alert("Styling applied on selected elements..!");
    } 
}

function apply_semantics_actn() {
    var custm_tag = document.getElementById("custm-tag").value;
    var custom_src = document.getElementById("custm-src").value;
    var custom_handler = document.getElementById("custm-handler").value;
    if(custm_tag === ""||custom_src === ""||custom_handler === ""){
        alert("Please enter all fields..!");
    }
    else{
        $.getScript(custom_src, function(){
            console.log("Request to script file success..!");
        })
        .done(function(script, jqxhr, textStatus) {
            var fn = window[custom_handler];
            fn();
        })
        .fail(function(jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request to script file failed: " + ", " + err);    
        })
        .always(function() {
            console.log("Request to script file complete");
        });
    }
}

function save_actn(elmnt_id) {
    var ac_pair_stmnt = {};
    var pgxform_stmnt = {
                            "loc":      {
                                        },
                            "ac_pair":  [
                                        ] 
                        };                       
    if(elmnt_id === "save-struct-actn-btn"){
        save_struct_actn(ac_pair_stmnt, pgxform_stmnt);
    }
    else if(elmnt_id === "save-styling-actn-btn"){
        save_styling_actn(ac_pair_stmnt, pgxform_stmnt);
    }
    else if(elmnt_id === "save-semantics-actn-btn"){
        save_semantics_actn(ac_pair_stmnt, pgxform_stmnt);
    }        
}

function save_struct_actn(ac_pair, pgxform) {
    var selctd_actn = document.getElementById("struct-actns-list").value;
    if(target_elems_data.length === 0){
        alert("Please select the Nodes..!");
    }
    else{
        switch(selctd_actn){
            case "addtxt": case "addimg": case "addyt": case "addnote":
                var src = document.getElementById('src-loc').value;
                var loc = document.getElementById('add-loc').value;
                if(src === ""){
                    alert("Please enter all fields..!");
                }
                else{
                    ac_pair[selctd_actn] = {};
                    ac_pair[selctd_actn]["srcloc"] = src;
                    ac_pair[selctd_actn]["addloc"] = loc;
                }
                break;

            case "addlink":
                var src = document.getElementById('src-loc').value;
                var loc = document.getElementById('add-loc').value;
                var link_txt = document.getElementById('link-txt').value;
                if(src === "" || link_txt === ""){
                    alert("Please enter all fields..!");
                }
                else{
                    ac_pair[selctd_actn] = {};
                    ac_pair[selctd_actn]["srcloc"] = src;
                    ac_pair[selctd_actn]["addloc"] = loc;
                    ac_pair[selctd_actn]["text"] = linktxt;   
                }
                break;

            case "lang-prcs":
                var from_lang = document.getElementById('select-from-lang').value;
                var to_lang = document.getElementById('select-to-lang').value;
                if(from_lang === to_lang){
                    alert("Please check the lang transformation selection..!");   
                }
                else{
                    ac_pair[selctd_actn] = {};
                    ac_pair[selctd_actn]["from_lang"] = from_lang;
                    ac_pair[selctd_actn]["to_lang"] = from_lang;
                }
                break;

            default:
                console.log("Error in save_struct_actn function, invalid id"+" "+elmnt_id+" "+"..!");
        }
        update_sss_pgxform(ac_pair, pgxform);    
    }
}

function save_styling_actn(ac_pair, pgxform) {
    if(target_elems_data.length === 0){
        alert("Please select the Nodes..!");
    }
    else{
        var fontfamily = document.getElementById("fontfamily-list").value;
        var fontweight = document.getElementById("fontweight-list").value; 
        var fontstyle = document.getElementById("fontstyle-list").value; 
        var textcolor = document.getElementById("textcolors-list").value;
        var fontsize = document.getElementById("fontsize-list").value;
        ac_pair["modelm"] = {};
        ac_pair["modelm"]["fontfamily"] = fontfamily;
        ac_pair["modelm"]["fontweight"] = fontweight;
        ac_pair["modelm"]["fontstyle"] = fontstyle;
        ac_pair["modelm"]["textcolor"] = textcolor;
        ac_pair["modelm"]["fontsize"] = fontsize;
        update_sss_pgxform(ac_pair, pgxform);
    }
}

// function save_semantics_actn(ac_pair, pgxform) {
//     var custm_tag = document.getElementById("custm-tag").value;
//     var custom_src = document.getElementById("custm-src").value;
//     var custom_handler = document.getElementById("custm-handler").value;
//     if(custm_tag === ""||custom_src === ""||custom_handler === ""){
//         alert("Please enter all fields");
//     }
//     else{
//         ac_pair["mod"] = {};
//     }
// }

function update_sss_pgxform(ac_pair, pgxform) {
    // var pgxform_length = sss["pgxform"].length;
    // if(pgxform_length > 0){
    //     for (var i = 0; i <= pgxform_length; i++){
    //         if (JSON.stringify(paths_list) === JSON.stringify(sss["pgxform"][i]["loc"]["node"])) {
    //             console.log("They are equal..!");
    //             sss["pgxform"][i]["ac_pair"].push(ac_pair);
    //             break;
    //         }
    //     }
    // }
    // else {
    //     pgxform["loc"]["node"] = paths_list;
    //     pgxform["ac_pair"].push(ac_pair);
    //     sss["pgxform"].push(pgxform);
    // }
    pgxform["loc"]["node"] = paths_list;
    pgxform["ac_pair"].push(ac_pair);
    sss["pgxform"].push(pgxform);
    alert("Saved action in ADM..!");
}

function check_modifications(elmnt_id) {
    var pgxform_length = sss["pgxform"].length;
    if(pgxform_length === 0) {
        alert("No modifications to save...!");
    }
    else {
        show_renform(elmnt_id);
    }
}

function update_sss_metadata(desc, name, id) {
    var urlParams = new URLSearchParams(window.location.search);
    sss.metadata["renurl"] = urlParams.get('foruri');   
    sss.metadata["sssdesc"] =  desc;
    sss.metadata["sssname"] = name;
    sss.metadata["sssid"] = id;
}

function update_sss_ssscrite(user, cmnty) {
    sss.ssscrite["usrname"] = user;
    sss.ssscrite["usrcmnty"] = cmnty;
}

function save_sss() {
    $.ajax(
        {
            url: config_data["SSS_DB_API_URL"],
            type: "POST",
            data: JSON.stringify(sss),
            dataType: 'json',
            async: false,
            contentType: 'application/json; charset=utf-8',
            success: function(res) {
                console.log(JSON.stringify(res)); 
                alert("Changes on page are saved successfully..! ☺️");
            }
        }
    );
}

function validate_sss_infoform() {
    var sss_desc = document.getElementById("sss-desc").value;
    var sss_name = document.getElementById("sss-name").value;
    var sss_id = document.getElementById("sss-id").value;
    var sss_username = document.getElementById("sss-username").value;
    var sss_cmntyname = document.getElementById("sss-cmntyname").value;
    if(sss_desc === "" || sss_id === "" || sss_username === "" || sss_cmntyname === "") {
        alert("All fields are required..!");
    }
    else{
        update_sss_metadata(sss_desc, sss_name, sss_id);
        update_sss_ssscrite(sss_username, sss_cmntyname);
        alert("Form is successfully submitted..! ☺️");
        alert(JSON.stringify(sss, null, 2));
        save_sss();
    }
}

function getElementByXpath(src_xpath){
    var ele = document.evaluate(src_xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    return ele;
}

function add_node(struct_actn, add_loc, new_text){
    var span = "<span style='background-color:yellow;'>"+new_text+"</span>";
    var xpath_data_len = target_elems_data.length;
    for(var i=0; i<xpath_data_len; i++){
        var elem = getElementByXpath(target_elems_data[i]["xpath"]);
        var start = target_elems_data[i]["startPoint"];
        var end = target_elems_data[i]["endPoint"];
        var elem_str = elem.innerText;
        var res = elem_str.substring(0, end) +" "+span+" "+ elem_str.substring(end + 1);
        elem.innerHTML = res;
        // if(add_loc === "below"){
        //     elem.insertAdjacentHTML('afterend', span);
        // }
        // else{
        //     elem.insertAdjacentHTML('beforebegin', span);
        // }        
    }
    alert(struct_actn+" "+"action applied on selected elements..!");
}

function addimg_node(sruct_actn, add_loc, img_src){
    var img_node = '<img src="'+img_src+'">';
    add_node(sruct_actn, add_loc, img_node);
}

function addyt_node(sruct_actn, add_loc, vdo_id){
    var embedHTML = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+vdo_id+'" frameborder="0" allowfullscreen></iframe>';
    add_node(sruct_actn, add_loc, embedHTML);
}

function addlink_node(sruct_actn, add_loc, url, text){
    var linkHTML = '<a href="'+url+'" target="_blank">'+text+'</a>';
    add_node(sruct_actn, add_loc, linkHTML);
}

function addnote_node(sruct_actn, add_loc, note_text){
    var cont_style ='width: 960px;margin: 100px auto;';
    var note_style = 'float:left;margin: auto;width: 300px;background: yellow;background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#EBEB00), to(#C5C500));background: -moz-linear-gradient(100% 100% 90deg, #C5C500, #EBEB00);padding: 20px 20px 20px 20px;-webkit-box-shadow: 0px 10px 30px #000;-moz-box-shadow: 0px 10px 30px #000';
    var noteHTML = '<div id="note_container" style="'+cont_style+'"><div class="stick_note" style="'+note_style+'"><p>'+note_text+'</div></div></div>';
    add_node(sruct_actn, add_loc, noteHTML);
}

function lang_translation(sruct_actn, from, to){
    var xpath_data_len = target_elems_data.length;
    for(var i=0; i<xpath_data_len; i++){
        var url = config_data["LANG_TRANS_API_URL"];
        var elem = getElementByXpath(target_elems_data[i]["xpath"]);
        var start = target_elems_data[i]["startPoint"];
        var end = target_elems_data[i]["endPoint"];
        var elem_str = elem.innerText;
        var str_to_translate = elem_str.substring(start, end);
        var xhr = new XMLHttpRequest();
        xhr.open("POST",url,true);
        xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
        xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        xhr.send(JSON.stringify( {"sentence":str_to_translate, "from-language":from, "to-language":to} ));
        xhr.onreadystatechange = function() {
            if (this.readyState==4 && this.status==200) {
                var res = this.responseText;
                elem.innerHTML = elem_str.substring(0, end) +" "+res+" "+ elem_str.substring(end + 1);
            }
        }
    }
}

